<?php

require 'connect.php';
$pdo = connect();

$stmt = $pdo->prepare('
	SELECT * FROM students
');

$stmt->execute();

$result = $stmt->fetchAll();

$message = '';

if (isset($_COOKIE['was_deleted'])) {
	$message = 'Студент был удален';
	setcookie('was_deleted');
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Студенты</title>
</head>
<body>
	<pre><?php print_r($_SESSION); ?></pre>
	<pre><?php print_r(headers_list()); ?></pre>
	<pre><?php print_r(getallheaders()); ?></pre>

	<a href="/create.php">Добавление студента</a>

	<div><?php echo $message ?></div>

	<table>
		<thead>
			<tr>
				<th>Имя</th>
				<th>Отчество</th>
				<th>Фамилия</th>
				<th>Средний бал</th>
				<th>День рождения</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($result as $item): ?>
			<tr>
				<td><?php echo $item['first_name'] ?></td>
				<td><?php echo $item['second_name'] ?></td>
				<td><?php echo $item['last_name'] ?></td>
				<td><?php echo $item['average_estimate'] ?></td>
				<td><?php echo $item['birthday'] ?></td>
				<td><a href="/update.php?id=<?php echo $item['id'] ?>">Обновить</a></td>
				<td><a href="/delete.php?id=<?php echo $item['id'] ?>">Удалить</a></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</body>
</html>
