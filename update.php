<?php 

$id = $_GET['id'];

require 'connect.php';

$pdo = connect();

$stmt = $pdo->prepare('SELECT * FROM students WHERE id = :id');
$stmt->execute(['id' => $id]);

$student = $stmt->fetch();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$stmt = $pdo->prepare('
		UPDATE students 
		SET
			first_name = :first_name, 
			second_name = :second_name, 
			last_name = :last_name,
			average_estimate = :average_estimate,
			birthday = :birthday
		WHERE id = :id
	');

	$isSucceed = $stmt->execute([
		'id' => $id,
		'first_name' => $_POST['fname'], 
		'second_name' => $_POST['sname'], 
		'last_name' => $_POST['lname'],
		'average_estimate' => $_POST['avg_est'],
		'birthday' => $_POST['birthday'],
	]);

	if ($isSucceed) {
		header('Location: /index.php');
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Обновление студента</title>
</head>
<body>
	<a href="/">Вернуться к списку</a>

	<form method="POST" style="display:flex;flex-direction:column;width:300px">
		<input type="text" name="fname" placeholder="Имя" value="<?php echo $student['first_name']?>" />
		<input type="text" name="sname" placeholder="Отчество" value="<?php echo $student['second_name']?>" />
		<input type="text" name="lname" placeholder="Фамилия" value="<?php echo $student['last_name']?>" />
		<input type="text" name="avg_est" placeholder="Средний бал" value="<?php echo $student['average_estimate']?>" />
		<input type="text" name="birthday" placeholder="День рождения" value="<?php echo $student['birthday']?>" />
		<button>Обновить</button>
	</form>
</body>
</html>
