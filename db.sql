create database if not exists college;

use college;

create table if not exists students(
	id int not null auto_increment,
	first_name varchar(40) not null,
	second_name varchar(40) default null,
	last_name varchar(40) not null,
	birthday date not null,
	average_estimate decimal(2,1) not null,
	primary key (id)
) default charset=utf8;

truncate students;

insert into students (first_name, second_name, last_name, birthday, average_estimate) values 
	('Иван', 'Сергеевич', 'Иванов', '1992-03-11', 3.5),
	('Алексей', 'Александрович', 'Петров', '1993-05-16', 5.0),
	('Анна', 'Сергеевна', 'Семенова', '1991-12-22', 3.6),
	('Григорий', 'Кириллович', 'Алексеев', '1995-09-21', 4.1),
	('Мао', null, 'Ли', '1994-09-11', 4.9),
	('Мария', 'Петровна', 'Ильина', '1996-12-19', 4.2),
	('Иван', 'Семенович', 'Иванчук', '1997-11-13', 4.7),
	('Елена', 'Александровна', 'Смыслова', '1992-10-01', 4.0),
	('Евгений', 'Петрович', 'Камский', '1991-02-05', 3.9),
	('Ольга', 'Ивановна', 'Сердюкова', '1992-03-31', 3.8),
	('Семен', 'Сергеевич', 'Симахин', '1990-10-30', 4.3),
	('Наталья', 'Федоровна', 'Суворова', '1989-08-29', 4.5);
