<?php

function connect () {
	$pdo = new PDO('mysql:dbname=college;hostname=localhost;charset=utf8', 'root', null);

	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	return $pdo;
}
