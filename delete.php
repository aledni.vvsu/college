<?php

require 'connect.php';

$pdo = connect();

$stmt = $pdo->prepare('DELETE FROM students WHERE id = :id');

$successResult = '1';

try {
	$isSuccess = $stmt->execute(['id' => $_GET['id']]);

	if ( ! $isSuccess) {
		$successResult = '0';
	}

	setcookie('was_deleted', 1);
} catch (Exception $e) {
	$successResult = '0';
}

header('Location: index.php');
