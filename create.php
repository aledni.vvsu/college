<?php

require 'connect.php';


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$pdo = connect();

	$stmt = $pdo->prepare('
		INSERT INTO students (
			first_name, 
			second_name, 
			last_name,
			average_estimate,
			birthday
		) VALUES (
			:first_name, 
			:second_name, 
			:last_name,
			:average_estimate,
			:birthday
		)
	');

	$isSucceed = $stmt->execute([
		'first_name' => $_POST['fname'], 
		'second_name' => $_POST['sname'], 
		'last_name' => $_POST['lname'],
		'average_estimate' => $_POST['avg_est'],
		'birthday' => $_POST['birthday']

	]);

	if ($isSucceed) {
		header('Location: /index.php');
	}
}



?>

<!DOCTYPE html>
<html>
<head>
	<title>Добавление студента</title>
</head>
<body>
	<a href="/index.php">Вернуться к списку</a>

	<form method="POST" style="display: flex; flex-direction: column; width:300px">
		<input type="" name="fname" placeholder="Имя" />
		<input type="" name="sname" placeholder="Отчество" />
		<input type="" name="lname" placeholder="Фамилия" />
		<input type="" name="avg_est" placeholder="Средний бал" />
		<input type="" name="birthday" placeholder="День рождения" />

		<button>Отправить</button>
	</form>
</body>
</html>
